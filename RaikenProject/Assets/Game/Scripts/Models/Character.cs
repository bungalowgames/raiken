﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterModel 
{
	public string objectID;
	public string characterName;
	public float HP;
	public float MP;
	public int level;
	public float damage;
	public float dodge;
	public float block;
	public float critical;
}

public class Character : MonoBehaviour {
	[SerializeField]
	private CharacterModel characterData;
	[SerializeField]
	private float currentHP;
	[SerializeField]
	private float currentMP;



	public void SetCurrentHP(float hp) { currentHP = hp; }
	public void SetCurrentMP(float mp) { currentMP = mp; }

	public void SetName(string name) { characterData.characterName = name; }
	public void SetHP(float hp) { characterData.HP = hp; }
	public void SetMP(float mp) { characterData.MP = mp; }
	public void SetLevel(int lvl) { characterData.level = lvl; }
	public void SetDamage(float dmg) { characterData.damage = dmg; }
	public void SetBlock(float blk) { characterData.block = blk; }
	public void SetCritical(float crit) { characterData.critical = crit; }

	public string GetObjectID() { return characterData.objectID; }
	public string GetName() { return characterData.characterName; }
	public float GetHP() { return characterData.HP; }
	public float GetMP() { return characterData.MP; }
	public int GetLevel() { return characterData.level; }
	public float GetDamage() { return characterData.damage; }
	public float GetBlock() { return characterData.block; }
	public float GetCritical() { return characterData.critical; }
}
